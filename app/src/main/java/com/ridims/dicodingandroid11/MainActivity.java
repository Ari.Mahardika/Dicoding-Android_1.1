package com.ridims.dicodingandroid11;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText edit_panjang, edit_lebar;
    private Button btn_hitung;
    private TextView txt_luas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Hitung Luas Persegi Panjang");
        edit_panjang = (EditText) findViewById(R.id.edit_panjang);
        edit_lebar = (EditText) findViewById(R.id.edit_lebar);
        btn_hitung = (Button) findViewById(R.id.btn_hitung);
        txt_luas = (TextView) findViewById(R.id.txt_luas);

        btn_hitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String panjang = edit_panjang.getText().toString().trim();
                String lebar = edit_lebar.getText().toString().trim();
                if (panjang.equals(".") || lebar.equals(".") || panjang.isEmpty() || lebar.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Input pada Panjang / Lebar tidak sesuai / kosong, masukkan angka", Toast.LENGTH_SHORT).show();
                } else {
                    double p = Double.parseDouble(panjang);
                    double l = Double.parseDouble(lebar);
                    double luas = p * l;
                    txt_luas.setText("Luas : " + luas);

                }
            }
        });
    }
}
